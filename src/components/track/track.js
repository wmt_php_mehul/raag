import React, { Component } from 'react'
import axios from 'axios';
import ReactPaginate from 'react-paginate';


export default class Track extends Component {
    constructor() {
        super();
        this.state = {
          track: [],
          loading: true,
          pageNo: 1,
        };
      }
      componentDidMount() {
      this.gettrack(); 
      }

      gettrack(){
          axios.get(`http://ws.audioscrobbler.com/2.0/?method=chart.gettoptracks&api_key=${ process.env.REACT_APP_SECRET_KEY}&format=json&limit=10&page=${this.state.pageNo}`).then(res => this.setState({ track: res.data.tracks.track }))
      
console.log(`http://ws.audioscrobbler.com/2.0/?method=chart.gettoptracks&api_key=4f78ebabb6e8fb9183bc22616a7a4946&format=json&limit=10&page=${this.state.pageNo}`)
      }
      handlePageClick = data => {
      
        this.setState({ pageNo: data.selected+1 }, () => {
          this.gettrack();
        });
      };

    render() {
      
    return (

        <div className="container ">
                    <div className="row">

         {       this.state.track.map((track, key) => 

                  <div className="col-xs-12 col-sm-6 col-md-4" key={key}>
                    <div className="card">
                        <div className="card-body text-center">
                            <a href={track.url } target='_blank'>
                                <img className="img-cirlce " src={track.image[2]['#text']} alt="card image" />
                               
                               <span> <h4 className="card-title">{track.name}</h4> </span>

                           <p className="card-text text-muted ">  {track.listeners}
                           
                            </p>
                            <div className="btn btn-primary btn-sm">Play Now</div> </a>
                        </div>
                    </div>
                </div>
   




)
}

</div>

<div className='pagination  col-xs-12 col-sm-6 col-md-4'>
<ReactPaginate 
          previousLabel={'previous'}
          nextLabel={'next'}
          breakLabel={'...'}
          breakClassName={'break-me'}
          pageCount={10}
          marginPagesDisplayed={2}
          pageRangeDisplayed={5}
          onPageChange={this.handlePageClick}
          containerClassName={'pagination'}
          subContainerClassName={'pages pagination'}
          activeClassName={'active'}
        />
</div>
</div>

);
}
}
