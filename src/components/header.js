import React, { Component } from 'react'
import logo from './image/boombox.svg'


export default function Header() {
  return (
    <div>
    <div className='container text-center'>
      <img src={logo} className='logo' />
        <h3>RaaG</h3>
        <p><em>We Too Love Music! </em></p>  
      </div>
    </div>
  )
}
