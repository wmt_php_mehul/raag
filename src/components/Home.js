import React, { Component } from 'react'
import Chart1 from './image/romantic-music.svg'
import Chart2 from './image/gramophone.svg'
import Chart3 from './image/guitar.svg'
import Artist from './artist/artist'
import Track from './track/track'
import Tag from './Tag/Tag'
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Redirect } from 'react-router-dom'

import { BrowserRouter as Router, Route, Link } from "react-router-dom";

export class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      artist: [],
      search: '',
    };
    this.search = this.search.bind(this);
  }

  async search(event) {

    await this.setState({ search: event.target.value });
    await axios.get(`http://ws.audioscrobbler.com/2.0/?method=artist.search&artist=${this.state.search}&api_key=${process.env.REACT_APP_SECRET_KEY}&format=json&limit=10`).then(res => this.setState({ artist: res.data.results.artistmatches.artist }))

  }
  renderRedirect = () => {
    return <Redirect to='/' />

  }
  render() {
    return (
      <div>

        <Router>
          <div className="container text-center">

            <div className='row '>
              <div className='col-sm-12'>
                <input type="text" placeholder="Search Artist" onKeyUp={this.search}  />
              </div>
              <div className='col-sm-4'>
                <Link to="/TopTrack"> <img src={Chart2} alt="logo" className="img-circel person d-none d-sm-block" />
                  <p><strong>Top Track</strong></p></Link>
              </div>
              <div className='col-sm-4'>
                <Link to="/TopArtist"> <img src={Chart3} alt="logo" className="img-circle person  d-none d-sm-block" />

                  <p><strong>Top Artists</strong></p></Link>
              </div>
              <div className='col-sm-4'>
                <Link to="/TopTag">   <img src={Chart1} alt="logo" className="img-circle person  d-none d-sm-block" />
                  <p><strong>Top Tag</strong></p></Link>
              </div>
              <div>
              </div>

              {this.state.search ?
                <div className="container">
                  {this.renderRedirect()}
                  <div className="row">
                    {this.state.artist.map((artist, key) =>


                      <div className="col-xs-12 col-sm-6 col-md-4" key={key}>
                        <div className="card">
                          <div className="card-body text-center">
                            <a href={artist.url} target='_blank'>
                              <img className="img-cirlce " src={artist.image[2]['#text']} alt="card image" />
                              <h4 className="card-title">{artist.name}</h4>

                            </a><p className="card-text text-muted "> <FontAwesomeIcon icon='headphones' /> {artist.listeners}
                            </p>

                          </div>
                        </div>
                      </div>


                    )
                    }
                  </div>
                  </div> 
                  : <h1></h1>}



              <Route path="/TopArtist" component={Artist} />
              <Route path="/TopTrack" component={Track} />
              <Route path="/TopTag" component={Tag} />
            </div>

          </div>
        </Router>


      </div>

    )
  }
}

export default Home;

