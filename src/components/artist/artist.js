import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import axios from 'axios';
import ReactPaginate from 'react-paginate';

export class Artist extends Component {
    constructor() {
        super();
        this.state = {
          artist: [],
          loading: true,
          pageNo: 1,
    
        };
      }
      componentDidMount() {
       this.getartist(); 
      }
      getartist(){
        axios.get(`http://ws.audioscrobbler.com/2.0/?method=chart.gettopartists&api_key=${ process.env.REACT_APP_SECRET_KEY}&format=json&limit=10&page=${this.state.pageNo}`).then(res => this.setState({ artist: res.data.artists.artist }))
     }
    handlePageClick = data => {
    
      this.setState({ pageNo: data.selected+1 }, () => {
        this.getartist();
      });
    };
    render() {
        return (
             <div className="container">
                    <div className="row">
        
           { this.state.artist.map((artist, key) => 
              

                    <div className="col-xs-12 col-sm-6 col-md-4" key={key}>
                        <div className="card">
                            <div className="card-body text-center">
                                <a href={artist.url } target='_blank'>
                                    <img className="img-cirlce " src={artist.image[2]['#text']} alt="card image" />
                                    <h4 className="card-title">{artist.name}</h4>

                                </a><p className="card-text text-muted "> <FontAwesomeIcon icon='headphones'/> {artist.listeners}
                                </p>
                                
                            </div>
                        </div>
                    </div>
            

            )
        }
        </div>

<div className='pagination col-xs-12 col-sm-6 col-md-4'>
<ReactPaginate 
          previousLabel={'previous'}
          nextLabel={'next'}
          breakLabel={'...'}
          breakClassName={'break-me'}
          pageCount={10}
          marginPagesDisplayed={2}
          pageRangeDisplayed={5}
          onPageChange={this.handlePageClick}
          containerClassName={'pagination'}
          subContainerClassName={'pages pagination'}
          activeClassName={'active'}
        />
</div>
        </div>
            );
    }
}

export default Artist
