import React, { Component } from 'react'
import axios from 'axios';
export default class Tag extends Component {

    constructor() {
        super();
        this.state = {
          tag: [],
          loading: true,
            
        };
      }
      componentDidMount() {
        axios.get(`http://ws.audioscrobbler.com/2.0/?method=chart.gettoptags&api_key=${ process.env.REACT_APP_SECRET_KEY}&format=json&limit=10`).then(res => this.setState({ tag: res.data.tags.tag }))
       
      }
    render() {
      
    return (
        this.state.tag.map((tag, key) => {
            return (
 
  
    <div className="col-xs-12 col-sm-6 col-md-4" key={key}> 
                    <div className="card">
                        <div className="card-body text-center">
                            <a href={tag.url } target='_blank'>
                               
                               <span> <h4 className="card-title">{tag.name}</h4> </span>

                           <p className="card-text text-muted ">  {tag.listeners}
                           
                            </p>
                            <div className="btn btn-primary btn-sm">Play Now</div> </a>
                        </div>
                    </div>
                </div>
   

);


}));
}
}
