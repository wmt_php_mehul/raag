import React, { Component } from 'react';
import './global.css';
import Home from './components/Home'
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './components/header'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faHeadphones } from '@fortawesome/free-solid-svg-icons'
library.add(faHeadphones)
class App extends Component {

  render() {

    return (
      <React.Fragment>
        <Header />
        <Home  />

      </React.Fragment>
      
    );
  }
}

export default App;
